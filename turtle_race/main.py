from turtle import Turtle, Screen
from random import randrange

screen = Screen()

# todo 1: placer le turtle dans la ligne de depart
position = [(-355, -100), (-355, -50), (-355, 0), (-355, 50), (-355, 100)]
color_list = ["red", "blue", "pink", "purple", "green"]
# todo 2: créer une fonction qui genère 6 turtles
def turtles(x, y, color):
    t = Turtle()
    t.penup()
    t.speed("fastest")
    t.shape("turtle")
    t.color(color)
    t.goto(x, y)
    return t


races_turtles = []

for pos in position:
    x = pos[0]
    y = pos[1]
    t = turtles(x, y, color_list[position.index(pos)])
    races_turtles.append(t)

user_choice = screen.textinput("Turtle", "The color of your player:")
user = "on"
while user != "off":
    for t in races_turtles:
        t.forward(randrange(10))
        if t.xcor() >= 350:
            user = "off"
            win = t.fillcolor()



if user_choice == win:
    print("You win!")
else:
    print(f"Is {win} the winner!")
print(win)

screen.mainloop()


# Projet Blackjack en Python

Ce projet est une implémentation simplifiée du jeu de Blackjack en Python. Le jeu est joué dans la console et simule une partie entre un joueur et un croupier.

## Comment jouer

Lancez le script `blackjack.py` dans votre environnement Python pour commencer à jouer. Le joueur et le croupier reçoivent deux cartes chacun, avec une des cartes du croupier face cachée. Le joueur a alors la possibilité de tirer des cartes supplémentaires ou de rester avec sa main actuelle. Le croupier révèle ensuite sa carte cachée et tire des cartes supplémentaires si nécessaire. Le gagnant est déterminé selon les règles standard du Blackjack.

## Fonctionnalités

- Création d'un jeu de cartes standard.
- Mélange et distribution des cartes.
- Calcul des scores pour le joueur et le croupier.
- Logique pour le tour du joueur et le tour du croupier.
- Détermination du gagnant de la partie.

## Règles du Blackjack

- Les cartes numériques valent leur nombre de points.
- Les valets, les reines et les rois valent 10 points.
- L'as peut valoir 1 ou 11 points.
- Le joueur peut tirer autant de cartes qu'il le souhaite jusqu'à ce qu'il décide de s'arrêter ou qu'il dépasse 21.
- Le croupier doit tirer des cartes jusqu'à ce que son total soit de 17 ou plus.
- Si le joueur ou le croupier dépasse 21, ils perdent la partie (bust).
- Si les deux ont 21 ou moins, le score le plus élevé gagne. En cas d'égalité, la partie est nulle.

## Comment contribuer

Les contributions sont les bienvenues! Si vous avez des suggestions ou des améliorations, veuillez créer une issue ou soumettre une pull request.

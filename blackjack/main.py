import random

def create_deck():
    """
    Crée un jeu de cartes standard de 52 cartes.
    """
    suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
    ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']
    return [{'Suit': suit, 'Rank': rank} for suit in suits for rank in ranks]

def shuffle_deck(deck):
    """
    Mélange le jeu de cartes.
    """
    random.shuffle(deck)
    return deck

def deal_card(deck):
    """
    Distribue la carte supérieure du jeu de cartes.
    """
    return deck.pop(0)

def initial_hand(deck):
    """
    Distribue deux cartes chacune au joueur et au croupier pour commencer le jeu.
    """
    return [deal_card(deck) for _ in range(2)], [deal_card(deck) for _ in range(2)]

def calculate_score(hand):
    """
    Calcule et retourne le score de la main donnée selon les règles du Blackjack.
    """
    values = {'2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10,
              'Jack': 10, 'Queen': 10, 'King': 10, 'Ace': 11}
    score = 0
    aces = 0

    for card in hand:
        rank = card['Rank']
        score += values[rank]
        if rank == 'Ace':
            aces += 1

    # Ajuster pour l'As si nécessaire
    while score > 21 and aces:
        score -= 10
        aces -= 1

    return score

def player_turn(deck, player_hand):
    """
    Gère le tour du joueur, où il peut continuer à tirer des cartes jusqu'à ce qu'il décide de s'arrêter
    ou qu'il dépasse 21.
    """
    while True:
        player_score = calculate_score(player_hand)
        if player_score < 17:
            player_hand.append(deal_card(deck))
        else:
            break
    return player_hand, calculate_score(player_hand)

def dealer_turn(deck, dealer_hand):
    """
    Gère le tour du croupier, qui doit tirer des cartes tant que son score est inférieur à 17.
    """
    while calculate_score(dealer_hand) < 17:
        dealer_hand.append(deal_card(deck))
    return dealer_hand, calculate_score(dealer_hand)

def determine_winner(player_score, dealer_score):
    """
    Détermine le gagnant de la main en comparant les scores du joueur et du croupier.
    """
    if player_score > 21:
        return "Le croupier gagne, le joueur a 'busté'!"
    elif dealer_score > 21:
        return "Le joueur gagne, le croupier a 'busté'!"
    elif player_score > dealer_score:
        return "Le joueur gagne!"
    elif player_score < dealer_score:
        return "Le croupier gagne!"
    else:
        return "Égalité! ('Push')"

def main():
    """
    La fonction principale pour exécuter une partie de Blackjack.
    """
    deck = create_deck()
    shuffled_deck = shuffle_deck(deck)
    player_hand, dealer_hand = initial_hand(shuffled_deck)
    player_hand, player_score = player_turn(shuffled_deck, player_hand)
    dealer_hand, dealer_score = dealer_turn(shuffled_deck, dealer_hand)
    print(f"Main du joueur: {player_hand}, score: {player_score}")
    print(f"Main du croupier: {dealer_hand}, score: {dealer_score}")
    winner = determine_winner(player_score, dealer_score)
    print(winner)

if __name__ == "__main__":
    # Exécute la fonction principale pour commencer le jeu si le script est exécuté directement.
    main()

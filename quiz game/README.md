# Quiz game

## Description
Quiz game est un jeu de quiz interactif en Python, qui utilise des questions de l'API Open Trivia Database. Ce projet permet aux utilisateurs de tester leurs connaissances dans divers domaines en répondant à des questions à choix multiples.

## Installation
Pour utiliser ce quiz, vous devez d'abord cloner ce dépôt et vous assurer que Python est installé sur votre machine.

## Utilisation
Exécutez le fichier `main.py` pour démarrer le quiz. Le jeu vous présentera une série de questions et vous devrez répondre par "Vrai" ou "Faux". Votre score sera affiché à la fin du quiz.

## Composants
Le projet se compose de plusieurs modules :

- `main.py` : Le point d'entrée du quiz, qui initialise le jeu et gère le flux de questions.
- `question_model.py` : Définit la classe `Question`, utilisée pour créer des instances de questions.
- `quiz_brain.py` : Contient la classe `QuizBrain` qui gère la logique du jeu, y compris le suivi des questions et des scores.
- `data` : Utilise l'API Open Trivia Database pour obtenir les questions de quiz.

## API
Les questions de quiz sont obtenues via l'API Open Trivia Database, accessible à [Open Trivia Database](https://opentdb.com/api_config.php).

## Contribution
Les contributions au projet sont les bienvenues. Veuillez vous assurer de suivre les bonnes pratiques de développement Python.

question_data = [
    {
        "type": "boolean",
        "difficulty": "hard",
        "category": "Science: Computers",
        "question": "The T-Mobile Sidekick smartphone is a re-branded version of the Danger Hiptop.",
        "correct_answer": "True",
        "incorrect_answers": [
            "False"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "The open source program Redis is a relational database server.",
        "correct_answer": "False",
        "incorrect_answers": [
            "True"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "A Boolean value of &quot;0&quot; represents which of these words?",
        "correct_answer": "False",
        "incorrect_answers": [
            "True"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "To bypass US Munitions Export Laws, the creator of the PGP published all the source code in book form. ",
        "correct_answer": "True",
        "incorrect_answers": [
            "False"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "hard",
        "category": "Science: Computers",
        "question": "The IBM PC used an Intel 8008 microprocessor clocked at 4.77 MHz and 8 kilobytes of memory.",
        "correct_answer": "False",
        "incorrect_answers": [
            "True"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "MacOS is based on Linux.",
        "correct_answer": "False",
        "incorrect_answers": [
            "True"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "easy",
        "category": "Science: Computers",
        "question": "The Windows ME operating system was released in the year 2000.",
        "correct_answer": "True",
        "incorrect_answers": [
            "False"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "easy",
        "category": "Science: Computers",
        "question": "The Windows 7 operating system has six main editions.",
        "correct_answer": "True",
        "incorrect_answers": [
            "False"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "The very first recorded computer &quot;bug&quot; was a moth found inside a Harvard Mark II computer.",
        "correct_answer": "True",
        "incorrect_answers": [
            "False"
        ]
    },
    {
        "type": "boolean",
        "difficulty": "medium",
        "category": "Science: Computers",
        "question": "The first computer bug was formed by faulty wires.",
        "correct_answer": "False",
        "incorrect_answers": [
            "True"
        ]
    }
]

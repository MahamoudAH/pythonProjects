# The Higher Lower Game - Edition Instagram

Ce projet est une implémentation en Python du célèbre jeu en ligne "The Higher Lower Game", avec une touche spéciale : il utilise des données inspirées d'Instagram. Le jeu consiste à deviner quelle personnalité ou quel élément a le plus grand nombre de followers sur Instagram.

## Fonctionnalités

- Jeu de devinettes basé sur des données inspirées d'Instagram.
- Comparaison de personnalités ou d'éléments basées sur leur popularité sur Instagram.
- Interface en ligne de commande avec art ASCII pour une expérience utilisateur attrayante.
- Score cumulatif pour chaque session de jeu.

## Installation

Aucune dépendance externe n'est requise, à part Python. 

1. Clonez le dépôt sur votre machine locale :
   ```
   git clone [url-du-dépôt]
   ```
2. Naviguez dans le dossier du projet.

## Utilisation

Exécutez le fichier `main.py` depuis la ligne de commande :

```
python main.py
```

Suivez les instructions à l'écran. Le jeu vous présentera deux éléments, et vous devrez deviner lequel a le plus de followers sur Instagram.

## Structure des Fichiers

- `main.py` : Contient la logique principale du jeu.
- `game_data.py` : Inclut les données simulées d'Instagram, sous forme de liste de dictionnaires.
- `art.py` : Contient des arts ASCII pour l'interface utilisateur.

## Données

Les données utilisées dans ce jeu sont fictives et servent uniquement à des fins de divertissement. Elles sont inspirées de profils et de statistiques d'Instagram mais ne reflètent pas nécessairement la réalité.

## Contribution

Les contributions, signalements de problèmes et demandes de nouvelles fonctionnalités sont les bienvenus. Suivez le processus standard de GitHub pour contribuer.

## Licence

Distribué sous la licence MIT.

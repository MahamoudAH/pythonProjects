# Dictionnaire représentant le menu avec les recettes et les coûts des boissons
MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

# Dictionnaire représentant les ressources disponibles pour préparer les boissons
resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}

# Fonction pour vérifier si les ressources nécessaires sont disponibles pour une boisson


def check_resources(user, MENU):
    for ingredient, required_amount in MENU[user]["ingredients"].items():
        if resources[ingredient] < required_amount:
            print(f"Sorry, there is not enough {ingredient}.")
            return False
    return True


# Boucle principale pour exécuter la machine à café
turn = "on"
while turn == "on":
    user = input("What would you like? (espresso/latte/cappuccino): ")

    # Traitement de la commande du client
    if user in ("espresso", "latte", "cappuccino"):
        print("Please insert coins.")
        quarters = float(input("how many quarters?: "))
        dimes = float(input("how many dimes?: "))
        nickles = float(input("how many nickles?: "))
        pennies = float(input("how many pennies?: "))
        coins = (0.25 * quarters) + (0.1 * dimes) + \
            (0.05 * nickles) + (0.01 * pennies)

        if coins >= MENU[user]["cost"]:
            if check_resources(user, MENU):
                print(f"Here is your {user} ☕. Enjoy!")
                # Mise à jour des ressources après préparation de la boisson
                for ingredient, amount in MENU[user]["ingredients"].items():
                    resources[ingredient] -= amount
        else:
            print("Sorry that's not enough money. Money refunded.")

    # Commande pour afficher le rapport des ressources
    if user == "report":
        for key, value in resources.items():
            unit = "ml" if key in ["water", "milk"] else "g"
            print(f"{key.capitalize()}: {value}{unit}")

    # Commande pour éteindre la machine
    if user == "off":
        turn = "off"

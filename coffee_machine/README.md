# Coffee Machine Simulator

Ce projet est une simulation simple d'une machine à café utilisant Python. Il permet aux utilisateurs de choisir entre espresso, latte, et cappuccino, calcule le coût, vérifie si suffisamment de ressources (eau, lait, café) sont disponibles, et traite les transactions monétaires.

## Fonctionnalités

- Choix entre trois types de boissons : espresso, latte, et cappuccino.
- Vérification de la disponibilité des ressources pour préparer la boisson choisie.
- Traitement des transactions monétaires avec calcul du changement.
- Fonction de rapport pour afficher les ressources actuelles de la machine.

## Prérequis

Aucune dépendance externe n'est nécessaire pour exécuter ce programme. Assurez-vous simplement que vous avez Python 3 installé sur votre système.

## Comment l'utiliser

1. Clonez le dépôt ou téléchargez le fichier `coffee_machine.py`.
2. Ouvrez votre terminal ou invite de commande.
3. Naviguez jusqu'au répertoire contenant `coffee_machine.py`.
4. Exécutez le script avec la commande `python coffee_machine.py`.
5. Suivez les instructions à l'écran pour choisir une boisson et insérer de l'argent.

## Commandes

- **Choix de boisson** : Tapez `espresso`, `latte`, ou `cappuccino` lorsque demandé.
- **Insertion de monnaie** : Entrez le nombre de chaque pièce (quarters, dimes, nickels, pennies) lorsque demandé.
- **Rapport** : Tapez `report` pour afficher les ressources actuelles de la machine.
- **Éteindre la machine** : Tapez `off` pour arrêter le programme.

## Contribution

Les contributions, suggestions et rapports de bugs sont les bienvenus. Pour contribuer, veuillez ouvrir une issue ou une pull request.

## Licence

Ce projet est sous licence [MIT](LICENSE).

## Auteurs

- ALI HASSAN Mahamoud

---

Bonne dégustation avec notre simulateur de machine à café ☕!

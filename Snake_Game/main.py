from turtle import Turtle, Screen

screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("Black")
screen.title("My Snake Game")


# todo 1: Create a snake body
# todo 2: Move the snake
# todo 3: Control Snake
# todo 4: Detect Collision with food
# todo 5: Create a Scoreboard
# todo 6: Detect collision with wall
# todo 7: Detect collision with tail.

screen.exitonclick()
# Importation des classes nécessaires depuis les modules appropriés
from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

# Création des objets pour la machine à café, le menu et la gestion de l'argent
coffee_maker = CoffeeMaker()
menu = Menu()
money_machine = MoneyMachine()

# Initialisation de la variable pour contrôler l'état de la machine (allumée ou éteinte)
is_on = "on"
while is_on != "off":  # Boucle tant que la machine est allumée
    # Demande à l'utilisateur de choisir une boisson dans le menu
    choice = input(f"What would you like? {menu.get_items()}: ")

    # Gestion des choix de l'utilisateur
    if choice == "off":
        is_on = "off"  # Éteint la machine si l'utilisateur entre 'off'
    elif choice == "report":
        coffee_maker.report()  # Affiche le rapport des ressources de la machine à café
        money_machine.report()  # Affiche le rapport financier
    else:
        # Trouve la boisson demandée dans le menu
        drink = menu.find_drink(choice)

        # Vérifie si les ressources sont suffisantes et si le paiement est effectué
        if coffee_maker.is_resource_sufficient(drink) and money_machine.make_payment(drink.cost):
            coffee_maker.make_coffee(drink)  # Prépare la boisson si les conditions sont remplies

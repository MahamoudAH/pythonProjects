# Simulateur de Machine à Café - Projet POO

## Description
Ce projet de simulateur de machine à café est une réalisation personnelle visant à tester et démontrer mes compétences en programmation orientée objet (POO). En simulant le fonctionnement interactif d'une machine à café, ce projet illustre l'utilisation pratique des principes de la POO tels que l'encapsulation, l'héritage, et le polymorphisme dans un contexte réel et convivial.

## Fonctionnalités
- **Menu Interactif** : Permet aux utilisateurs de choisir parmi plusieurs types de boissons caféinées.
- **Simulation de Paiement** : Intègre une simulation réaliste du processus de paiement.
- **Rapports Dynamiques** : Fournit des rapports sur l'état actuel de la machine, démontrant la gestion d'état.
- **Extensibilité** : Facilité d'ajouter ou de modifier des boissons et fonctionnalités, illustrant la flexibilité de la POO.

## Installation
Ce projet ne requiert aucune installation spécifique. Assurez-vous d'avoir Python installé sur votre système pour exécuter le simulateur.

## Utilisation
Lancez le simulateur avec la commande suivante :

```bash
python coffee_machine.py
```

Suivez les instructions à l'écran pour vivre l'expérience d'une machine à café virtuelle.

## Architecture du Projet
- `CoffeeMaker` : Gère la préparation des boissons et les ressources.
- `Menu` : Offre une interface pour choisir les boissons.
- `MoneyMachine` : Simule le traitement des transactions financières.

## Pourquoi ce Projet ?
Ce projet a été conçu comme un défi personnel pour appliquer et approfondir mes compétences en programmation orientée objet. Il représente une étape importante dans mon parcours d'apprentissage.

## Contributions
Les retours et contributions sont toujours appréciés pour améliorer le projet et encourager les pratiques de développement collaboratif.

## Licence
Ce projet est distribué sous licence libre. Vous êtes libre de l'utiliser, de le modifier et de le distribuer.
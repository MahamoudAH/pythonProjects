from turtle import Turtle, Screen
from random import choice

screen = Screen()
screen.colormode(255)

color_list = [(202, 164, 110), (240, 245, 241), (236, 239, 243), (149, 75, 50), (222, 201, 136), (53, 93, 123), (170, 154, 41), (138, 31, 20), (134, 163, 184), (197, 92, 73), (47, 121, 86), (73, 43, 35), (145, 178, 149), (14, 98, 70), (232, 176, 165), (160, 142, 158), (54, 45, 50), (101, 75, 77), (183, 205, 171), (36, 60, 74), (19, 86, 89), (82, 148, 129), (147, 17, 19), (27, 68, 102), (12, 70, 64), (107, 127, 153), (176, 192, 208), (168, 99, 102)]

t = Turtle()
t.hideturtle()
t.penup()
t.setheading(225)
t.forward(300)
t.setheading(0)

x_position = [-225, -175, -125, -75, -25, 25, 75, 125, 175, 225]

y_position = [-225, -175, -125, -75, -25, 25, 75, 125, 175, 225]

for y in y_position:
    for x in x_position:
        x = x
        y = y
        color = choice(color_list)
        t.speed("fastest")
        t.goto(x, y)
        t.dot(20, color)


t.screen.mainloop()
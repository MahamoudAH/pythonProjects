from random import randint
from art import logo

# Constants for the number of turns based on difficulty
EASY_LEVEL_TURNS = 10  # More turns for an easier difficulty level
HARD_LEVEL_TURNS = 5  # Fewer turns for a harder difficulty level


# Function to compare the player's guess to the actual number
def check_guess(guess, number):
    """Compare the player's guess against the number to guess.

    Args:
        guess (int): The player's guess.
        number (int): The number to guess.

    Returns:
        str: Outcome message indicating whether the guess was too high, too low, or correct.
    """
    if guess > number:
        return "Too high."
    elif guess < number:
        return "Too low."
    else:
        return "Correct!"


# Function to set the number of turns based on the chosen difficulty
def get_turns(difficulty):
    """Set the number of turns based on the difficulty level chosen by the player.

    Args:
        difficulty (str): The chosen difficulty level ('easy' or 'hard').

    Returns:
        int: The number of turns corresponding to the chosen difficulty level.
    """
    if difficulty == "easy":
        return EASY_LEVEL_TURNS
    else:
        return HARD_LEVEL_TURNS


# Main game function
def play_number_guessing_game():
    """Main function to run the number guessing game."""

    # Introduce the game to the player
    print(logo)
    print("Welcome to the Number Guessing Game!")
    print("I'm thinking of a number between 1 and 100.")

    # Generate a random number between 1 and 100 for the player to guess
    number = randint(1, 100)

    # Ask the player to choose a difficulty level and set the number of turns accordingly
    difficulty = input("Choose a difficulty. Type 'easy' or 'hard': ").lower()
    turns = get_turns(difficulty)

    # Initialize the player's guess to 0 (an impossible value for the correct answer)
    guess = 0

    # Loop until the player guesses the correct number or runs out of turns
    while guess != number:
        # Inform the player of the remaining number of attempts
        print(f"You have {turns} attempts remaining.")

        # Get the player's guess as an integer
        guess = int(input("Make a guess: "))

        # Check the guess and print the result
        result = check_guess(guess, number)
        print(result)

        # If the guess is incorrect, decrement the number of turns left
        if result != "Correct!":
            turns -= 1
            if turns == 0:
                print("You've run out of guesses. You lose.")
                break
        else:
            # If the guess is correct, congratulate the player
            print(f"You got it! The answer was {number}.")


play_number_guessing_game()
